/* Опишіть своїми словами що таке Document Object Model (DOM)
Це структуроване представлення документу (HTML, XML), що складається 
із структурованих вузлів та об'єктів, які мають свої властивості та методи.
Фактично, за допомогою DOM можна отримувати доступ до документу та змінювати його. 

Яка різниця між властивостями HTML-елементів innerHTML та innerText?
innerText - дозволяє отримувати доступ до тексту на сторінці (додати, змінити, видалити), не чіпає синтаксис HTML.
innerHTML - дозволяє додати (видалити) до документу новий елемент - текст + тег(атрибут)

Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
getElementById(id) (за ідентифікатором), querySelectorAll (за селектором CSS),
querySelector(за селектором CSS - повертає тільки перший результат)

Також застарілі: getElementsByTagName(за тегом), getElementsByClassName(за класом),
ElementsByName(за атрибутом name).

Також можна використати до дочінього та батьківського елемента, використавши відповідно child
та parent і похдні від них. А також можна знайти сусідні елементи.

Найьільш унівесальним є querySelector.

*/

// 1. Знайти всі параграфи на сторінці та встановити колір фону #ff0000

let allParagraphs = document.querySelectorAll('p');
allParagraphs.forEach(element => {
    element.style.backgroundColor = '#ff0000'
});

/* 2. Знайти елемент із id = "optionsList".
Вивести у консоль.Знайти батьківський елемент та вивести в консоль.
Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.
*/

let elementList = document.getElementById('optionsList');
console.log(elementList);

let parentList = elementList.parentElement;
console.log(parentList);

if (elementList.hasChildNodes) {
    elementList.childNodes.forEach(element => {
        console.log(element.nodeName);
        console.log(element.nodeType);
    });
}

// 3. Встановіть в якості контента елемента з класом testParagraph наступний параграф - This is a paragraph

let contentElementParagraph = document.getElementById('testParagraph');
contentElementParagraph.textContent = 'This is a paragraph';

/* Отримати елементи, вкладені в елемент із класом main - header і вивести їх у консоль.
Кожному з елементів присвоїти новий клас nav - item.
*/

let mainHeaderElements = document.querySelector('.main-header').children;
console.log(mainHeaderElements);

for (let element of mainHeaderElements) {
    element.classList.add('nav-item');
}

// 5. Знайти всі елементи із класом section - title. Видалити цей клас у цих елементів.

let sectionElements = document.querySelectorAll('.section-title');
sectionElements.forEach(element => {
    element.classList.remove('section-title');
});


